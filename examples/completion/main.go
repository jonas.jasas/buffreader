package main

import (
	"fmt"
	"gitlab.com/jonas.jasas/buffreader"
	"log"
	"net/http"
	"time"
)

func main() {
	if res, err := http.DefaultClient.Get("http://example.com/"); err == nil && res.StatusCode == 200 {
		br := buffreader.New(res.Body)

		go func() {
			// Waiting for buffering to end
			<-br.Buff()
			// Now source reader can be closed and all associated resources freed.
			log.Println("Buffering finished, closing reader")
			res.Body.Close()
			// At the same time consumer still consumes data
		}()

		log.Print("Slowly consuming data")
		for b, n, err := make([]byte, 50), 0, error(nil); err == nil; {
			n, err = br.Read(b)
			fmt.Print(string(b[:n]))
			time.Sleep(time.Millisecond * 100)
		}
	} else {
		log.Print("Unsuccessful request")
	}
}
