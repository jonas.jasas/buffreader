package main

import (
	"gitlab.com/jonas.jasas/buffreader"
	"gitlab.com/jonas.jasas/rwmock"
	"log"
	"time"
)

// Example illustrates buffering cancellation.
// prod is unlimited data stream. Reading just 10*100 bytes and cancelling buffering
// as no more data required.
func main() {
	// Generating unlimited data stream
	prod := rwmock.NewRandStream(0, 1, 100, time.Millisecond*10, time.Millisecond*10)

	log.Print("Buffering stream")
	br := buffreader.New(prod)
	br.Buff() // Start buffering

	d := make([]byte, 100)
	for i := 0; i < 10; i++ {
		time.Sleep(time.Millisecond * 100)
		br.Read(d)
		// Reading 10 times and finding out that "no more data is required".
	}

	log.Print("Cancelling buffering")
	br.Close()

	<-br.Buff() // Waiting for buffering to stop
	log.Print("Buffering is stopped")

	// Do things not warring that buffer is growing
}
