package main

import (
	"log"
)

// Example illustrates slow data consumer case.
// prod (data producer) generates data in higher rates comparing to cons (data consumer).
// prod is finishing faster leaving consumer to finish it's work.
func main() {

	a := []byte("liau")
	b := []byte("miau")
	b[0] = 60
	c := append(a, b...)

	log.Print(string(c))

	//log.Print("Starting to produce stream")
	//prod := rwmock.NewRandStream(100000, 1, 1000, time.Millisecond, time.Millisecond*10)
	//br := buffreader.New(prod)
	//
	//go func() {
	//	// Waiting for buffering to finish
	//	res := <-br.Buff()
	//	log.Printf("Buffering is finished. Bytes buffered: %d Error: %v", res.Size, res.Err)
	//}()
	//
	//// Blocking until consumer reads all transferred data
	//cons := rwmock.NewConsumer(time.Millisecond, time.Second)
	//
	//io.Copy(cons, br)
	//
	//log.Printf("Red from BuffReader: %d", cons.WBytes())
}
