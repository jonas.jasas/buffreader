// BuffReader is a Go package that implements thread safe and dynamically resized buffer for io.Reader.
// BuffReader increases data transfer performance between reader and writer.
// It makes sure that reader (e.g. network connection) never waits slow writer (e.g. slow data processing) by buffering received data in to memory.
package buffreader

import (
	"bytes"
	"errors"
	"gitlab.com/jonas.jasas/closechan"
	"io"
	"sync"
)

// BuffRes is used to return buffering result.
// Fields are the same as the result of io.Copy method.
type BuffRes struct {
	Size int64
	Err  error
}

// bytes.Buffer used internally for mocking in tests
type internalBuff interface {
	io.ReadWriter
	Reset()
	Len() int
}

var ErrClosedBuffReader = errors.New("BuffReader is closed")

type BuffReader struct {
	src         io.Reader
	buffer      internalBuff
	l           sync.Mutex
	closeChan   *closechan.CloseChan
	writeChan   *closechan.CloseChan
	buffResChan chan BuffRes
	err         error
}

// New function creates new BuffReader
// src - reader that will be red and data will be stored in to the internal buffer.
func New(src io.Reader) *BuffReader {
	return &BuffReader{
		src:       src,
		buffer:    &bytes.Buffer{},
		closeChan: closechan.NewCloseChan(),
		writeChan: closechan.NewCloseBufChan(1),
	}
}

// Read method implements standard io.Reader interface.
// If buffering is not started (with Buff()) Read() also starts background buffering.
func (br *BuffReader) Read(b []byte) (n int, err error) {
	if br.buffResChan == nil {
		br.Buff()
	}

	br.l.Lock()
	defer br.l.Unlock()
	for br.buffer.Len() == 0 {
		select {
		case <-br.buffResChan:
			return 0, br.err
		case <-br.closeChan.C():
			return 0, ErrClosedBuffReader
		default:
			br.l.Unlock()
			<-br.writeChan.C()
			br.l.Lock()
		}
	}

	return br.buffer.Read(b)
}

// Buff method starts buffering in subroutine (non-blocking).
// Returns channel that will return buffering result when buffering is finished.
// Method starts buffering only the first time it is called, so can be used to get channel multiple times.
// If method is called after buffering is done or Close() is called it returns closed channel.
//
// <-chan BuffRes - returns result of reading from input reader src
// size - is the number of bytes that were red from input reader.
// err - error that occurred during reading from input reader and writing to internal buffer. Err values:
// * there was no error err == nil
// * is closed err == ErrClosedBuffReader
// * unable to allocate memory for the buffer err == bytes.ErrTooLarge
func (br *BuffReader) Buff() <-chan BuffRes {
	select {
	case <-br.closeChan.C():
		t := make(chan BuffRes)
		close(t)
		return t
	default:
		br.l.Lock()
		defer br.l.Unlock()

		if br.buffResChan == nil {
			br.buffResChan = make(chan BuffRes)

			select {
			case <-br.closeChan.C():
				close(br.buffResChan)
			default:
				go br.copy()
			}
		}
		return br.buffResChan
	}
}

// Close stops loading buffer and releasing memory.
// Source reader stays open.
// Returns ErrClosedBuffReader error if Close() is called more than once.
func (br *BuffReader) Close() (err error) {
	if !br.closeChan.Close() {
		err = ErrClosedBuffReader
	} else {
		br.l.Lock()
		br.buffer.Reset()
		br.l.Unlock()
	}

	br.writeChan.Close()
	return
}

func (br *BuffReader) copy() {
	var size int64
	buf := make([]byte, 32*1024)
	for {
		nr, er := br.src.Read(buf)
		if nr > 0 {
			nw, ew := br.write(buf[0:nr])
			if nw > 0 {
				size += int64(nw)
			}
			if ew != nil {
				br.err = ew
				break
			}
		}
		if er != nil {
			br.err = er
			break
		}
	}

	// Broadcasting buffering result to all waiters
	res := BuffRes{Size: size, Err: br.err}
	if res.Err == io.EOF {
		res.Err = nil
	}
	for more := true; more; {
		select {
		case br.buffResChan <- res:
		default:
			more = false
		}
	}
	close(br.buffResChan)

	br.writeChan.Close()
}

func (br *BuffReader) write(b []byte) (n int, err error) {
	select {
	case <-br.closeChan.C():
		return 0, ErrClosedBuffReader
	default:
		br.l.Lock()
		defer func() {
			if r := recover(); r != nil {
				br.buffer.Reset()
				err = r.(error)
			}
			br.l.Unlock()
		}()

		n, err = br.buffer.Write(b)

		select {
		case br.writeChan.C() <- struct{}{}:
		default:
		}

		return
	}
}

// BUG(br): If BuffReader is closed while waiting on reading source io.Reader buffering subroutine will not exit.
// To avoid hanging subroutine close source io.Reader.
