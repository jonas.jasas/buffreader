package buffreader

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"math"
	"math/rand"
	"testing"
	"time"
)

func TestSlowConsumer(t *testing.T) {
	equivalent := make([]byte, 10000000)
	rand.Read(equivalent)
	r, w := io.Pipe()
	go func() {
		const step = 10000
		for i := 0; i <= len(equivalent)-step; i += step {
			w.Write(equivalent[i : i+step])
			time.Sleep(time.Microsecond)
		}
		w.Close()
	}()

	br := New(r)
	br.Buff()

	tmpBuff := make([]byte, 1000)
	resultBuff := bytes.Buffer{}

	var n int
	var err error
	for err == nil {
		n, err = br.Read(tmpBuff)
		resultBuff.Write(tmpBuff[:n])
		time.Sleep(time.Microsecond)
	}

	if bytes.Compare(equivalent, resultBuff.Bytes()) != 0 {
		t.FailNow()
	}
}

func TestPulsatingProducer(t *testing.T) {
	equivalent := make([]byte, 10000000)
	rand.Read(equivalent)
	r, w := io.Pipe()
	go func() {
		const step = 10000
		for i := 0; i <= len(equivalent)-step; i += step {
			w.Write(equivalent[i : i+step])
			delay := int((math.Sin(float64(i/step)/10) + 1) * 1000)
			time.Sleep(time.Microsecond * time.Duration(delay))
		}
		w.Close()
	}()

	br := New(r)

	tmpBuff := make([]byte, 1000)
	resultBuff := bytes.Buffer{}

	var n int
	var err error
	for err == nil {
		n, err = br.Read(tmpBuff)
		resultBuff.Write(tmpBuff[:n])
		time.Sleep(time.Microsecond)
	}

	if bytes.Compare(equivalent, resultBuff.Bytes()) != 0 {
		t.FailNow()
	}
}

func TestClose(t *testing.T) {
	r := rand.New(rand.NewSource(666))
	br := New(r)

	go func() {
		time.Sleep(time.Millisecond * 1)
		if br.Close() != nil {
			t.FailNow()
		}
	}()

	tmpBuff := make([]byte, 1000)
	var err error
	for err == nil {
		_, err = br.Read(tmpBuff)
		time.Sleep(time.Millisecond * 2)
	}

	if err != ErrClosedBuffReader {
		t.FailNow()
	}

	if br.Close() != ErrClosedBuffReader {
		t.FailNow()
	}
}

func TestCloseBeforeBuff(t *testing.T) {
	r := rand.New(rand.NewSource(666))
	lr := io.LimitReader(r, 1000)
	br := New(lr)

	if br.Close() != nil {
		t.FailNow()
	}

	select {
	case <-br.Buff():
	default:
		t.FailNow()
	}

	if br.Close() != ErrClosedBuffReader {
		t.FailNow()
	}
}

func TestCloseAfterBuff(t *testing.T) {
	r := rand.New(rand.NewSource(666))
	lr := io.LimitReader(r, 1000)
	br := New(lr)
	<-br.Buff()

	if br.Close() != nil {
		t.FailNow()
	}

	if br.Close() != ErrClosedBuffReader {
		t.FailNow()
	}
}

func TestCloseWhileRead(t *testing.T) {
	//src := rand.New(rand.NewSource(666))
	//lr := io.LimitReader(src, 1000)
	r, _ := io.Pipe()
	br := New(r)

	subrChan := make(chan error)
	go func() {
		b := make([]byte, 1000)
		var err error
		for err == nil {
			time.Sleep(time.Millisecond)
			_, err = br.Read(b)
		}
		subrChan <- err
	}()

	time.Sleep(time.Millisecond * 100)

	if br.Close() != nil {
		t.FailNow()
	}

	if <-subrChan != ErrClosedBuffReader {
		t.FailNow()
	}
}

func TestWaitBufferingResult(t *testing.T) {
	equivalent := make([]byte, 10000000)
	rand.Read(equivalent)
	r, w := io.Pipe()
	go func() {
		const step = 100000
		for i := 0; i <= len(equivalent)-step; i += step {
			w.Write(equivalent[i : i+step])
			time.Sleep(time.Millisecond)
		}
		w.Close()
	}()

	br := New(r)

	brRes := <-br.Buff()

	if brRes.Err != nil {
		t.FailNow()
	}

	if brRes.Size != int64(len(equivalent)) {
		t.FailNow()
	}
}

type panicBuffer struct {
	bytes.Buffer
}

func (b *panicBuffer) Write(p []byte) (n int, err error) {
	panic(bytes.ErrTooLarge)
}

func TestWritePanic(t *testing.T) {
	r := rand.New(rand.NewSource(666))
	br := New(r)
	br.buffer = &panicBuffer{}

	brRes := <-br.Buff()

	if brRes.Err != bytes.ErrTooLarge {
		t.FailNow()
	}

	rb := make([]byte, 100)
	if _, err := br.Read(rb); err != bytes.ErrTooLarge {
		t.FailNow()
	}
}

type readerWithErr struct {
	n int
}

var ErrTestBuffWriteErr = errors.New("Reader error")

func (r *readerWithErr) Read(p []byte) (n int, err error) {
	r.n++
	if r.n < 3 {
		n = len(p)
		rand.Read(p)
	} else {
		err = ErrTestBuffWriteErr
	}
	return
}

func TestSrcErr(t *testing.T) {
	r := &readerWithErr{}
	br := New(r)

	brRes := <-br.Buff()

	if brRes.Err != ErrTestBuffWriteErr {
		t.FailNow()
	}
}

func TestFinishedWriter(t *testing.T) {
	equivalent := make([]byte, 10000000)
	rand.Read(equivalent)
	r := bytes.NewReader(equivalent)
	br := New(r)
	brRes := <-br.Buff()

	if brRes.Err != nil {
		t.FailNow()
	}

	res, err := ioutil.ReadAll(br)

	if err != nil {
		t.FailNow()
	}

	if bytes.Compare(equivalent, res) != 0 {
		t.FailNow()
	}
}
