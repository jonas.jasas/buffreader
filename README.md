# Dynamic and thread safe byte buffer

BuffReader reads all available data from the input [io.Reader](https://golang.org/pkg/io/#Reader) and writes it in to the internal buffer asynchronously. 
BuffReader implements [io.Reader](https://golang.org/pkg/io/#Reader) interface for reading data from the internal buffer.

* Dynamic buffer size
* Thread safe buffer reading
* Cancellabe buffering routine
* Buffering completion notification

[![go report card](https://goreportcard.com/badge/gitlab.com/jonas.jasas/buffreader)](https://goreportcard.com/report/gitlab.com/jonas.jasas/buffreader)
[![pipeline status](https://gitlab.com/jonas.jasas/buffreader/badges/master/pipeline.svg)](https://gitlab.com/jonas.jasas/buffreader/commits/master)
[![coverage report](https://gitlab.com/jonas.jasas/buffreader/badges/master/coverage.svg)](https://gitlab.com/jonas.jasas/buffreader/commits/master)
[![godoc](https://godoc.org/gitlab.com/jonas.jasas/buffreader?status.svg)](http://godoc.org/gitlab.com/jonas.jasas/buffreader)


## When to use buffered reader?

* **Slow data producer** (e.g. network): you can start receiving data before you do something with it
* **Slow data consumer** (e.g. heavy data processing): producer is not going be blocked
* **Both consumer and producer slow**: data transfer and processing will be faster because consumer and producer will not wait each other


## Installation

Simple install the package to your [$GOPATH](https://github.com/golang/go/wiki/GOPATH "GOPATH") with the [go tool](https://golang.org/cmd/go/ "go command") from shell:
```bash
$ go get gitlab.com/jonas.jasas/buffreader
```
Make sure [Git is installed](https://git-scm.com/downloads) on your machine and in your system's `PATH`.


## Exaples

### Basic

Example show how to create buffered reader `br` from `myReader` ([io.Reader](https://golang.org/pkg/io/#Reader))

```go
// Creating buffered reader br  
br := buffreader.New(myReader)

br.Buff() // Start buffering
// At this point all data from myReader is getting red asynchronously in to the internal buffer.
// Slow operations can be done here not warring about blocking myReader.

// Read data from the buffered reader br
myConsumer(br)
```


### Buffering completion notification

Example shows how to take an action when buffering is finished or error occurred while buffering. 

```go
// Creating buffered reader br and channel resChan that will notify when buffering is done  
br := buffreader.New(myReader)

go func() {
    res := <-br.Buff()    // Blocking until buffering is finished
    if res.Err == nil {
        // Do something knowing that myReader is no longer needed (e.g. close network connection) 
    } else {
        // In case of error buffer will be freed immediately
        log.Printf("Buffering failed with the error: %v", res.Err)
    }
}()

// Read() method will return same error as in res.Err
myConsumer(br)
```

Full examples can be found in [examples](examples) directory.


## Buffer size

As you probably already noticed there is no buffer size limit parameter.
All memory that is available to the process will be used for buffering.
Read method and resChan returns error buffer.ErrTooLarge if it is not possible to allocate memory for the buffer.
Please make sure that in the worse case scenario all data is going to fit in memory.
Buffer limiting feature will be implemented in feature versions.